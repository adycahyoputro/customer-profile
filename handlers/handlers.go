package handlers

import (
	"customer-profile/middlewares"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func HandleRequest() {
	log.Println("Start server on port 8002!")

	myRouter := mux.NewRouter().StrictSlash(true)
	handler := cors.AllowAll().Handler(myRouter)

	myRouter.HandleFunc("/", HomePage)
	myRouter.HandleFunc("/users", middlewares.IsAuthorized(GetAllCustomers)).Methods("OPTIONS", "GET")
	myRouter.HandleFunc("/user/{userid}", middlewares.IsAuthorized(GetCustomer)).Methods("OPTIONS", "GET")
	myRouter.HandleFunc("/user", middlewares.IsAuthorized(InsertCustomer)).Methods("OPTIONS", "POST")
	myRouter.HandleFunc("/user/{id}", middlewares.IsAuthorized(UpdateCustomer)).Methods("OPTIONS", "PUT")
	myRouter.HandleFunc("/user/{id}", middlewares.IsAuthorized(DeleteCustomer)).Methods("OPTIONS", "DELETE")
	myRouter.HandleFunc("/user/login", Login).Methods("OPTIONS", "POST")
	log.Fatal(http.ListenAndServe(":8002", handler))
	// log.Fatal(http.ListenAndServe(":"+configs.GetMainPort(), handler))
}
