package handlers

import (
	"customer-profile/connection"
	"customer-profile/repositories"
	"customer-profile/structs"
	"customer-profile/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func HomePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Greeting!")
}

func GetAllCustomers(w http.ResponseWriter, r *http.Request) {
	pagination := utils.GeneratePagination(r)
	var user structs.USER
	userLists, error := repositories.GetAllUsers(&user, &pagination)

	if error != nil {
		fmt.Println(error.Error())
	}
	res := structs.Result{Code: 200, Data: userLists, Message: "Success get customers"}
	results, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(results)
}
func GetCustomers(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	limit, errorLimit := strconv.Atoi(vars["limit"])
	if errorLimit != nil {
		fmt.Println(errorLimit.Error())
	}
	offset, errorOffset := strconv.Atoi(vars["offset"])
	if errorOffset != nil {
		fmt.Println(errorOffset.Error())
	}

	customers := []structs.USER{}

	connection.DB.
		Select([]string{"name", "age"}).
		Limit(limit).
		Offset((offset - 1) * limit).
		Order("name asc").
		Find(&customers)

	res := structs.Result{Code: 200, Data: customers, Message: "Success get customers"}
	results, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(results)
}

func GetCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	customerID := vars["userid"]

	var risk_profile structs.RISK_PROFILE

	connection.DB.Where("userid = ?", customerID).First(&risk_profile)
	connection.DB.Model(&risk_profile).Select([]string{"id", "name", "age"}).Related(&risk_profile.MasterUser, "Userid")

	res := structs.Result{Code: 200, Data: risk_profile, Message: "Success get customers"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

func InsertCustomer(w http.ResponseWriter, r *http.Request) {
	//initialized parameter
	payloads, _ := ioutil.ReadAll(r.Body)
	var users []structs.USER

	//get data from frontend
	var customer structs.USER
	var risk_profile structs.RISK_PROFILE
	json.Unmarshal(payloads, &customer)
	customer_name := customer.Name

	//get data from backend/database
	var customer_table structs.USER
	connection.DB.Where("name = ?", customer_name).First(&customer_table)
	customer_name_db := customer_table.Name

	//checking user input
	if customer.Name != "" && customer.Age >= 0 && customer.Password != "" {
		//checking username exist
		if customer_name == customer_name_db {
			http.Error(w, "name has already exist", http.StatusUnauthorized)
		} else {
			// hashing password
			hash, errorHash := HashPassword(customer.Password)
			if errorHash != nil {
				http.Error(w, errorHash.Error(), http.StatusBadRequest)
			}
			customer.Password = hash
			//create user
			connection.DB.Create(&customer)

			//create risk profile
			user_age := customer.Age
			risk_profile.Userid = customer.ID
			difference_age := (55 - user_age)
			if difference_age >= 30 {
				risk_profile.Stock = 72.5
				risk_profile.Bond = 21.5
				risk_profile.MM = 100 - 72.5 - 21.5
			} else if difference_age >= 20 && difference_age < 30 {
				risk_profile.Stock = 54.5
				risk_profile.Bond = 25.5
				risk_profile.MM = 100 - 54.5 - 25.5
			} else {
				risk_profile.Stock = 34.5
				risk_profile.Bond = 45.5
				risk_profile.MM = 100 - 34.5 - 45.5
			}
			json.Unmarshal(payloads, &risk_profile)
			connection.DB.Create(&risk_profile)

			queryBuilder := connection.DB.Select([]string{"id", "name", "age"}).Limit(1)
			dataUser := queryBuilder.Model(&structs.USER{}).Where("name = ?", customer_name).Find(&users)
			res := structs.Result{Code: 200, Data: dataUser, Message: "Success insert customers"}
			result, err := json.Marshal(res)

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(result)
		}
	} else {
		http.Error(w, "invalid input", http.StatusBadRequest)
	}

}

func UpdateCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	customerID := vars["id"]

	payloads, _ := ioutil.ReadAll(r.Body)

	var customerUpdate structs.USER
	json.Unmarshal(payloads, &customerUpdate)

	var customer structs.USER
	connection.DB.Where("id = ?", customerID).First(&customer)
	connection.DB.Model(&customer).Updates(&customerUpdate)

	// risk_profile_id := vars["id"]
	var risk_profile_update structs.RISK_PROFILE

	user_age := customerUpdate.Age
	// risk_profile.Userid = customer.ID
	difference_age := (55 - user_age)
	if difference_age >= 30 {
		risk_profile_update.Stock = 72.5
		risk_profile_update.Bond = 21.5
		risk_profile_update.MM = 100 - 72.5 - 21.5
	} else if difference_age >= 20 && difference_age < 30 {
		risk_profile_update.Stock = 54.5
		risk_profile_update.Bond = 25.5
		risk_profile_update.MM = 100 - 54.5 - 25.5
	} else {
		risk_profile_update.Stock = 34.5
		risk_profile_update.Bond = 45.5
		risk_profile_update.MM = 100 - 34.5 - 45.5
	}
	json.Unmarshal(payloads, &risk_profile_update)

	var risk_profile structs.RISK_PROFILE
	connection.DB.Where("id = ?", customerID).First(&risk_profile)
	connection.DB.Model(&risk_profile).Updates(&risk_profile_update)

	res := structs.Result{Code: 200, Data: customer, Message: "Success update customers"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

func DeleteCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	customerID := vars["id"]

	var customer structs.USER
	connection.DB.Where("id = ?", customerID).First(&customer)
	connection.DB.Delete(&customer)

	var risk_profile structs.RISK_PROFILE
	connection.DB.Where("id = ?", customerID).First(&risk_profile)
	connection.DB.Delete(&risk_profile)

	res := structs.Result{Code: 200, Data: customer, Message: "Success delete customers"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}
