package handlers

import (
	"customer-profile/connection"
	"customer-profile/structs"
	"encoding/json"
	"os"
	"time"

	// "fmt"
	"io/ioutil"
	"log"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	// "github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func Login(w http.ResponseWriter, r *http.Request) {
	payloads, _ := ioutil.ReadAll(r.Body)

	// var users []structs.USER

	//get data from frontend
	var customer_user structs.USER
	json.Unmarshal(payloads, &customer_user)
	customer_name := customer_user.Name
	customer_password := customer_user.Password

	//get data from backend/database
	var customer_table structs.USER
	connection.DB.Where("name = ?", customer_name).First(&customer_table)
	customer_name_db := customer_table.Name
	customer_password_hash := customer_table.Password
	customer_id := customer_table.ID

	//get token table
	// var users_token []structs.Token
	var token_table structs.Token

	//checking user input
	if customer_name != "" && customer_password != "" {
		//checking username exist
		if customer_name != customer_name_db {
			http.Error(w, "name doesn't exist", http.StatusUnauthorized)
		} else {
			//checking password
			match := CheckPasswordHash(customer_password, customer_password_hash)
			if !match {
				http.Error(w, "wrong password", http.StatusUnauthorized)
			} else {
				//generate jwt
				token, err := CreateToken(customer_id)
				if err != nil {
					http.Error(w, err.Error(), http.StatusUnprocessableEntity)
					return
				}
				token_table.Userid = customer_id
				token_table.Token = token
				// connection.DB.Create(&token_table)
				connection.DB.Where("userid = ?", customer_id).First(&token_table)
				connection.DB.Model(&token_table).Select([]string{"id", "name", "age"}).Related(&token_table.MasterUser, "Userid")

				// queryBuilder := connection.DB.Select([]string{"userid", "token"}).Limit(1)
				// dataUser := queryBuilder.Model(&structs.Token{}).Where("userid = ?", customer_id).Find(&users_token)
				// dataUser := connection.DB.Select("id", "name", "age").Where("name = ?", customer_name).First(&customer_user)
				res := structs.Result{Code: 200, Data: token_table, Message: "Success login customers"}
				result, err := json.Marshal(res)

				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				w.Write(result)
			}
		}
	} else {
		http.Error(w, "invalid input", http.StatusBadRequest)
	}

}

func CreateToken(userid int) (string, error) {
	var err error
	os.Setenv("ACCESS_SECRET", "secret")
	// sign := jwt.New(jwt.GetSigningMethod("HS256"))
	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["userid"] = userid
	claims["exp"] = time.Now().Add(time.Minute * 15).Unix()
	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := sign.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return "", err
	}
	return token, nil
}
