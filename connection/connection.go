package connection

import (
	"customer-profile/structs"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	DB  *gorm.DB
	Err error
)

func Connect() {
	// DB, Err = gorm.Open("mysql", "root:@/customer_profile?charset=utf8&parseTime=True")

	DB, Err = gorm.Open("mysql", "adyputro:gKeB1Um25uShQXbT@/adyputro?charset=utf8&parseTime=True")

	if Err != nil {
		log.Println("Connection failed", Err)
	} else {
		log.Println("Server success")
	}

	// DB.Exec("PRAGMA foreign_keys = ON;")
	DB.AutoMigrate(&structs.USER{}, &structs.RISK_PROFILE{}, &structs.Token{})
	// DB.AutoMigrate(&structs.RISK_PROFILE{})
	// DB.AutoMigrate(&structs.Token{})
	// var risk_profile structs.RISK_PROFILE
	DB.Model(&structs.RISK_PROFILE{}).AddForeignKey("userid", "users(id)", "CASCADE", "CASCADE")
	DB.Model(&structs.Token{}).AddForeignKey("userid", "users(id)", "CASCADE", "CASCADE")
	// DB.Model(&structs.USER{}).Related(&risk_profile, "RISK_PROFILE")
}
