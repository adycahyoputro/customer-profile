package repositories

import (
	"customer-profile/connection"
	"customer-profile/structs"
)

func GetAllUsers(user *structs.USER, pagination *structs.Pagination) (*[]structs.USER, error) {
	var users []structs.USER
	offset := (pagination.Page - 1) * pagination.Limit
	queryBuilder := connection.DB.Select([]string{"id", "name", "age"}).Limit(pagination.Limit).Offset(offset).Order(pagination.Sort)
	result := queryBuilder.Model(&structs.USER{}).Where(user).Find(&users)
	if result.Error != nil {
		message := result.Error
		return nil, message
	}
	return &users, nil
}
