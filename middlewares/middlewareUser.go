package middlewares

import (
	"fmt"
	"net/http"
	"os"

	"github.com/dgrijalva/jwt-go"
)

func IsAuthorized(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//checking token exist
		if r.Header["Token"] == nil {
			http.Error(w, "No Token Found", http.StatusUnauthorized)
			return
		}
		//parsing token
		os.Setenv("ACCESS_SECRET", "secret")
		var mySigningKey = []byte(os.Getenv("ACCESS_SECRET"))
		// var mySigningKey = []byte("secret")
		token, err := jwt.Parse(r.Header["Token"][0], func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("There was an error in parsing")
			}
			return mySigningKey, nil
		})
		//checking expired token
		if err != nil {
			http.Error(w, "Your Token has been expired or invalid token", http.StatusUnauthorized)
			return
		}
		//checking verified token
		if token != nil && err == nil {
			fmt.Println("token verified")
			handler.ServeHTTP(w, r)
			return
		} else {
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
		}
		// if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		// 	if claims["role"] == "admin" {

		// 		r.Header.Set("Role", "admin")
		// 		handler.ServeHTTP(w, r)
		// 		return

		// 	} else if claims["role"] == "user" {

		// 		r.Header.Set("Role", "user")
		// 		handler.ServeHTTP(w, r)
		// 		return
		// 	}
		// }
		// var reserr Error
		// reserr = SetError(reserr, "Not Authorized")
		// json.NewEncoder(w).Encode(err)

	}
}
