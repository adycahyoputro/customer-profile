This collection contains customer-profile requests API using GO language.

It contains following request:

- Create user
- Get Pagination user
- Get user by ID
- Update user
- Delete user

## Available Script

Inside project directory, you can run:

### `go run main.go`

Check if the app running, <br />
Open [http://localhost:8000](http://localhost:8000) on browser.
