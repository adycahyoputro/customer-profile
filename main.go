package main

import (
	"customer-profile/connection"
	"customer-profile/handlers"
)

func main() {

	connection.Connect()

	handlers.HandleRequest()
}
