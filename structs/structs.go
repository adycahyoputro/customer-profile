package structs

// import "github.com/jinzhu/gorm"

//table user
type USER struct {
	ID       int    `gorm:"primaryKey" json:"id"`
	Name     string `json:"name"`
	Age      int    `json:"age"`
	Password string `json:"password"`
	// RISK_PROFILE RISK_PROFILE `gorm:"ForeignKey:Userid"`
}

//table risk-profile
type RISK_PROFILE struct {
	// gorm.Model
	ID     int `json:"id"`
	Userid int `json:"userid"`
	// USER   USER `gorm:"ForeignKey:Userid;AssociationForeignKey:Name"`
	MasterUser USER    `gorm:"ForeignKey:Userid"`
	MM         float32 `json:"mm"`
	Bond       float32 `json:"bond"`
	Stock      float32 `json:"stock"`
}

//nofitication table user and risk profile
type Result struct {
	Code    int         `json:"code"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

//table pagination
type Pagination struct {
	Limit int    `json:"limit"`
	Page  int    `json:"page"`
	Sort  string `json:"sort"`
}

//table token
type Token struct {
	// ID         int    `json:"id"`
	Userid     int    `json:"userid"`
	MasterUser USER   `gorm:"ForeignKey:Userid"`
	Token      string `json:"token"`
}
